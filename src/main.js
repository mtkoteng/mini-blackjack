import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from './router'

Vue.use(VueRouter)
Vue.config.productionTip = false

new Vue({
  router,
  data:{
    name:''
  },
  mounted(){
    if(localStorage.name){
      this.name = localStorage.name;
    }
  },
  watch:{
    name(newName){
      localStorage.name = newName;
    }
  },
  render: h => h(App),
}).$mount('#app')
