import HomeScreen from "./components/HomeScreen.vue";
import MainSite from "./components/MainSite.vue";
import VueRouter from 'vue-router'
const routes = [
    {
        path: '/',
        component: HomeScreen,
    },
    {
        path: '/play/:playerName',
        component: MainSite
    }
]

const router = new VueRouter({routes})
export default router