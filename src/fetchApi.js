export function fetchApi(){
    //Add deck_count as a GET or POST parameter to define the number of Decks you want to use.
    // Blackjack typically uses 6 decks. The default is 1.
    const url = 'https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1'
    return fetch(url)
        .then(response => response.json())
}

export function drawCardFromApi(deckId, numOfCards){
    const url = `https://deckofcardsapi.com/api/deck/${deckId}/draw/?count=${numOfCards}`
    console.log(url)
    return fetch(url)
        .then(response => response.json())
}